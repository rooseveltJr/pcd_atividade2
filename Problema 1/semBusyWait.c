
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int request = 0;
int respond = 0;
int soma = 0;

void *client_thread(void * id)
{
    int i;
    int local;
    i = (int) id;
    while(1)
    {
        // while(respond != i)
        // {
        //     request = i;
        // }
        local = soma;
        printf("\n<---------------------------------------\n");
        printf("ENTRANDO SECAO CRITICA : %d\n SOMA: %d\n", i, soma);
        sleep(rand()%2);
        soma = local + 1;
        printf("SAINDO SECAO CRITICA : %d\n SOMA: %d\n", i, soma);
        printf("\n--------------------------------------->\n");
        respond = 0;
    }
}

void server()
{
    while(1)
    {
        while(request == 0){};
        respond = request;
        while(respond != 0){};
        request = 0;
    }

}

int main(void)
{
    pthread_t *threads;
    int n_threads;
    int i;
    printf("Insira a quantidade de clientes: \n");
    scanf("%d", &n_threads);

    threads = (pthread_t *) malloc(sizeof(pthread_t) * n_threads);
    for(i = 0 ; i < n_threads ; i++)
    {
        pthread_create(&threads[i], NULL, client_thread, (void * ) (i+1));
    }
    server();
    return 0;
}