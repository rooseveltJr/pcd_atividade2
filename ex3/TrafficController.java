//public class TrafficController {

//   public void enterLeft() {}
//    public void enterRight() {}
//    public void leaveLeft() {}
//    public void leaveRight() {}
//}


import java.util.ArrayDeque;
import java.util.Queue;


public class TrafficController {

    Queue<Thread> queue;

    public TrafficController() {
        queue = new ArrayDeque<Thread>();
    }

    public synchronized void enterLeft() {
        queue.add(Thread.currentThread());
        while (queue.peek() != Thread.currentThread()) {
            try {
                wait();
            } catch (InterruptedException ex) {}
        }
    }

    public synchronized void enterRight() {
        queue.add(Thread.currentThread());
        while (queue.peek() != Thread.currentThread()) {
            try {
                wait();
            } catch (InterruptedException ex) {}
        }
    }

    public synchronized void leaveLeft() {
        if (queue.poll() != Thread.currentThread()) {
            System.err.close();
        }
        notifyAll();
    }

    public synchronized void leaveRight() {
        if (queue.poll() != Thread.currentThread()) {
            System.err.close();
        }
        notifyAll();
    }
}
